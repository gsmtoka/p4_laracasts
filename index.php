<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE-edge">
    	<meta name="viewposrt" content="width=device-width, initial-scale=1">

    	<title>CXX for Backend Devs</title>

    	<link href="https://fonts.googleapis.com/css?family=Baloo:400,500" rel="stylesheet">
    	<link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/app.css">
    </head>
    <body>
        <header>
            <div class="container">
                <div class="header-top">
                    <h1>GsmToka</h1>
                    <h3 align="center">Filter Employes by Age</h3>
                    <a href="#">Sign In</a>
                </div>



                <nav>
				<a href="?filter=less_than_50_years&id=1" id="1">UNDER 50 YEARS</a>
                <a href="?filter=under_zero_salary&id=2"  id="2">UNDER 0 SALARY</a>
                <a href="?filter=more_than_200_years&id=3" id="3" >200+ YEARS</a>
                <a href="?filter=from_100_to_150_years&id=4" id="4">100-150 YEARS</a>               
                </nav>
            </div>
        </header>

    <script>
	    var get = <?php echo $_GET['id'] ?>;
	    var id = document.getElementById(get);
	    id.style.color = "black";
	</script>

        <div id="background-wrap">
            <div class="x1">
                <div class="cloud"></div>
            </div>

            <div class="x2">
                <div class="cloud"></div>
            </div>

            <div class="x3">
                <div class="cloud"></div>
            </div>

            <div class="x4">
                <div class="cloud"></div>
            </div>

            <div class="x5">
                <div class="cloud"></div>
            </div>
        </div>


        <div class="music"> <h1 style="margin-top: 0px; color: #BCD5DA">PUSH TO PLAY :)) &nbsp (მუსიკა გათიშულია დავალიანების გამო) &nbsp</h1>
            <audio controls autoplay>
            <source src="ArxotisCasavit.mp3" type="audio/mpeg">
        </audio></div>

        <div>
			<?php 
			  	    $employees = json_decode(file_get_contents('http://dummy.restapiexample.com/api/v1/employees?fbclid=IwAR1IuL34aOax7gGhWxlWH6wWqy6abFgk3bVOCvhcvk5TJaaeSkFwKv5GGM8'), true);

$page = isset($_GET['filter']) ? $_GET['filter'] : false;

switch($page){
    case 'less_than_50_years': 
    foreach($employees as $each){
        if($each['employee_age'] < 50){
            echo $each['employee_name']." is ".$each['employee_age']." years old and has salary ".$each['employee_salary']."<br>";
        }
    }
    break;

    case 'under_zero_salary': 
    foreach($employees as $each){
        if($each['employee_salary'] < 0){
            echo $each['employee_name']." is ".$each['employee_age']." years old and has salary ".$each['employee_salary']."<br>";
        }
    }
    break;

    case 'more_than_200_years': 
    foreach($employees as $each){
        if($each['employee_age'] > 200){
            echo $each['employee_name']." is ".$each['employee_age']." years old and has salary ".$each['employee_salary']."<br>";
        }
    }
    break;

    case 'from_100_to_150_years': 
    foreach($employees as $each){
        if($each['employee_age'] >= 100 && $each['employee_age'] <= 150){
            echo $each['employee_name']." is ".$each['employee_age']." years old and has salary ".$each['employee_salary']."<br>";
        }
    }
    break;

    default:
        echo "<h1 style='color:red;'> Select filter in order to view desirable results </h1>";
    }					  
            ?>
		</div>
        
            
    </body>
</html>